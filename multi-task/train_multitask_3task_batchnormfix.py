import sys
import caffe

#script to train 3 task caffe solver

#LIST OF BATCH NORMALIZATION LAYERS FOR SHARING - caffe batch normalization weight sharing is broken, this is a hot-fix
#remove layers that you don't want shared
L = [('conv1/bn','conv1/bn_p','conv1/bn_p2'),('scale1','scale1_p','scale1_p2'),
     ('conv2/bn','conv2/bn_p','conv2/bn_p2'),('scale2','scale2_p','scale2_p2'),
     ('conv3/bn','conv3/bn_p','conv3/bn_p2'),('scale3','scale3_p','scale3_p2'),
     ('conv4/bn','conv4/bn_p','conv4/bn_p2'),('scale4','scale4_p','scale4_p2')]


if __name__ == '__main__':
        caffe.set_device(0)
        caffe.set_mode_gpu()
        solver = caffe.SGDSolver('./models/ssolver_multitask_3task_fc2.prototxt')

	Niterations = 300000 #precomupted number of iterations
        for i in range(Niterations):
                sys.stdout.flush()

		#CONSTRAINED LAYER
            	tmp = solver.net.params['convF'][0].data*10000 #get contrained filter weights
            	tmp[:,:,2,2] = 0 #set middle value to 0
            	tmp = tmp.reshape((3,1,1,25))
            	tmp = tmp/tmp.sum(3).reshape((3,1,1,1)) #normalize
            	tmp = tmp.reshape((3,1,5,5))
            	tmp[:,:,2,2] = -1 #set middle value to 1
            	solver.net.params['convF'][0].data[...] = tmp #set constrained filter weights
            	solver.net.params['convF'][1].data[...] = 0
            	solver.step(1) #iterate

	        #Checks to make sure that layers through share depth match, and that layers above share depth are updating
            	print 'conv2bn: ' + str(solver.net.params['conv2/bn'][0].data[0]) + ' | conv2bn_p: ' + str(solver.net.params['conv2/bn_p'][0].data[0]) + ' | conv2bn_p2: ' + str(solver.net.params['conv2/bn_p2'][0].data[0])
                print 'fc6: ' + str(solver.net.params['fc6'][0].data[0][0]) + ' | fc6_p: ' + str(solver.net.params['fc6_p'][0].data[0][0]) + ' | fc6_p2: ' + str(solver.net.params['fc6_p2'][0].data[0][0])


		#AVERAGE AND COPY LAYERS IN L (hotfix for batch norm sharing)
		for l in L:
		    for j in range(len(solver.net.params[l[0]])):
			d0 = solver.net.params[l[0]][j].data #get data from first leg
			d1 = solver.net.params[l[1]][j].data #get data from 2nd leg
			d2 = solver.net.params[l[2]][j].data #get data from 3rd leg
			d = (d0+d1+d2)/3.0 #average them
			#copy into all legs
			solver.net.params[l[0]][j].data[...] = d
			solver.net.params[l[1]][j].data[...] = d
			solver.net.params[l[2]][j].data[...] = d
		


                print 'iteration ' + str(i+1) + ' is done'
