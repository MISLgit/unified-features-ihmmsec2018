name: "MISLNet_Multi"
layer {
  name: "datacam"
  type: "Data"
  top: "datacam"
  top: "labelcam"
  include {
    phase: TRAIN
  }
  data_param {
    source: "/data/owen/lmdb_traincam_20cam6proc_400k"
    batch_size: 40
    backend: LMDB
  }
}
layer {
  name: "dataproc"
  type: "Data"
  top: "dataproc"
  top: "labelproc"
  include {
    phase: TRAIN
  }
  data_param {
    source: "/data/owen/multi_bel_proc6/train_lmdb"
    batch_size: 40
    backend: LMDB
  }
}
layer {
  name: "datacam"
  type: "Data"
  top: "datacam"
  top: "labelcam"
  include {
    phase: TEST
  }
  data_param {
    source: "/data/owen/lmdb_valcam_20cam6proc_30k"
    batch_size: 15
    backend: LMDB
  }
}
layer {
  name: "dataproc"
  type: "Data"
  top: "dataproc"
  top: "labelproc"
  include {
    phase: TEST
  }
  data_param {
    source: "/data/owen/multi_bel_proc6/val_lmdb"
    batch_size: 25
    backend: LMDB
  }
}
layer {
  name: "convF"
  type: "Convolution"
  bottom: "datacam"
 top: "convF"
  param {
    name: "convF_w"
    lr_mult: 1
    decay_mult: 1
  }
  param {
    name: "convF_b"
    lr_mult: 2
    decay_mult: 0
  }
  convolution_param {
    num_output: 3
    kernel_size: 5
    stride: 1
    weight_filler {
      type: "xavier"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 0.1
    }
  }
}
layer {
  name: "conv1"
  type: "Convolution"
  bottom: "convF"
  top: "conv1"
  param {
    name: "conv1_w"
    lr_mult: 1
    decay_mult: 1
  }
  param {
    name: "conv1_b"
    lr_mult: 2
    decay_mult: 0
  }
  convolution_param {
    num_output: 96
    pad: 3
    kernel_size: 7
    stride: 2
    weight_filler {
      type: "xavier"
    }
    bias_filler {
      type: "constant"
      value: 0.1
    }
  }
}
layer {
  name: "conv1/bn"
  type: "BatchNorm"
  bottom: "conv1"
  top: "conv1/bn"
  param {
    name: "conv1/bn_a"
    lr_mult: 0
  }
  param {
    name: "conv1/bn_b"
    lr_mult: 0
  }
  param {
    name: "conv1/bn_c"
    lr_mult: 0
  }
}
layer {
  name: "scale1"
  type: "Scale"
  bottom: "conv1/bn"
  top: "scale1"
  scale_param {
    bias_term: true
  }
}

layer {
  name: "relu1"
  type: "TanH"
  bottom: "scale1"
  top: "scale1"
}
layer {
  name: "pool1"
  type: "Pooling"
  bottom: "scale1"
  top: "pool1"
  pooling_param {
    pool: MAX
    kernel_size: 3
    stride: 2
  }
}
layer {
  name: "conv2"
  type: "Convolution"
  bottom: "pool1"
  top: "conv2"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  convolution_param {
    num_output: 64
    pad: 2
    kernel_size: 5
    #stride: 2
    weight_filler {
      type: "xavier"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 0.1
    }
  }
}
layer {
  name: "conv2/bn"
  type: "BatchNorm"
  bottom: "conv2"
  top: "conv2/bn"
  param {
    lr_mult: 0
  }
  param {
    lr_mult: 0
  }
  param {
    lr_mult: 0
  }
}
layer {
  name: "scale2"
  type: "Scale"
  bottom: "conv2/bn"
  top: "scale2"
  scale_param {
    bias_term: true
  }
}
layer {
  name: "relu2"
  type: "TanH"
  bottom: "scale2"
  top: "scale2"
}
layer {
  name: "pool2"
  type: "Pooling"
  bottom: "scale2"
  top: "pool2"
  pooling_param {
    pool: MAX
    kernel_size: 3
    stride: 2
  }
}
layer {
  name: "conv3"
  type: "Convolution"
  bottom: "pool2"
  top: "conv3"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  convolution_param {
    num_output: 64
    pad: 2
    kernel_size: 5
    #stride: 2
    weight_filler {
      type: "xavier"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 0.1
    }
  }
}
layer {
  name: "conv3/bn"
  type: "BatchNorm"
  bottom: "conv3"
  top: "conv3/bn"
  param {
    lr_mult: 0
  }
  param {
    lr_mult: 0
  }
  param {
    lr_mult: 0
  }
}
layer {
  name: "scale3"
  type: "Scale"
  bottom: "conv3/bn"
  top: "scale3"
  scale_param {
    bias_term: true
  }
}
layer {
  name: "relu3"
  type: "TanH"
  bottom: "scale3"
  top: "scale3"
}
layer {
  name: "pool3"
  type: "Pooling"
  bottom: "scale3"
  top: "pool3"
  pooling_param {
    pool: MAX
    kernel_size: 3
    stride: 2
  }
}
layer {
  name: "conv4"
  type: "Convolution"
  bottom: "pool3"
  top: "conv4"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  convolution_param {
    num_output: 128
    kernel_size: 1
    weight_filler {
      type: "xavier"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 0.1
    }
  }
}
layer {
  name: "conv4/bn"
  type: "BatchNorm"
  bottom: "conv4"
  top: "conv4/bn"
  param {
    lr_mult: 0
  }
  param {
    lr_mult: 0
  }
  param {
    lr_mult: 0
  }
}
layer {
  name: "scale4"
  type: "Scale"
  bottom: "conv4/bn"
  top: "scale4"
  scale_param {
    bias_term: true
  }
}
layer {
  name: "relu4"
  type: "TanH"
  bottom: "scale4"
  top: "scale4"
}
layer {
  name: "pool4"
  type: "Pooling"
  bottom: "scale4"
  top: "pool4"
  pooling_param {
    pool: AVE
    kernel_size: 3
    stride: 2
  }
}
layer {
  name: "fc6"
  type: "InnerProduct"
  bottom: "pool4"
  top: "fc6"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  inner_product_param {
    num_output: 200
    weight_filler {
      type: "xavier"
      std: 0.005
    }
    bias_filler {
      type: "constant"
      value: 1
    }
  }
}
layer {
  name: "relu6"
  type: "TanH"
  bottom: "fc6"
  top: "fc6"
}

layer {
  name: "fc7"
  type: "InnerProduct"
  bottom: "fc6"
  top: "fc7"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  inner_product_param {
    num_output: 200
    weight_filler {
      type: "xavier"
      std: 0.005
    }
    bias_filler {
      type: "constant"
      value: 1
    }
  }
}
layer {
  name: "relu7"
  type: "TanH"
  bottom: "fc7"
  top: "fc7"
}

layer {
  name: "fc8"
  type: "InnerProduct"
  bottom: "fc7"
  top: "fc8"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  inner_product_param {
    num_output: 20
    weight_filler {
      type: "xavier"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 0
    }
  }
}
layer {
  name: "accuracy_cam"
  type: "Accuracy"
  bottom: "fc8"
  bottom: "labelcam"
  top: "accuracy_cam"
  include {
    phase: TEST
  }
}
layer {
  name: "loss_cam"
  type: "SoftmaxWithLoss"
  bottom: "fc8"
  bottom: "labelcam"
  top: "loss_cam"
  loss_weight: 1
}

###---                              ---###
###---MANIPULATION DETECTION NETWORK---###
###---                              ---###

layer {
  name: "convF_p"
  type: "Convolution"
  bottom: "dataproc"
 top: "convF_p"
  param {
    name: "convF_w"
    lr_mult: 1
    decay_mult: 1
  }
  param {
    name: "convF_b"
    lr_mult: 2
    decay_mult: 0
  }
  convolution_param {
    num_output: 3
    kernel_size: 5
    stride: 1
    weight_filler {
      type: "xavier"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 0.1
    }
  }
}
layer {
  name: "conv1_p"
  type: "Convolution"
  bottom: "convF_p"
  top: "conv1_p"
  param {
    name: "conv1_w"
    lr_mult: 1
    decay_mult: 1
  }
  param {
    name: "conv1_b"
    lr_mult: 2
    decay_mult: 0
  }
  convolution_param {
    num_output: 96
    pad: 3
    kernel_size: 7
    stride: 2
    weight_filler {
      type: "xavier"
    }
    bias_filler {
      type: "constant"
      value: 0.1
    }
  }
}
layer {
  name: "conv1/bn_p"
  type: "BatchNorm"
  bottom: "conv1_p"
  top: "conv1/bn_p"
  param {
    name: "conv1/bn_a"
    lr_mult: 0
  }
  param {
    name: "conv1/bn_b"
    lr_mult: 0
  }
  param {
    name: "conv1/bn_c"
    lr_mult: 0
  }
}
layer {
  name: "scale1_p"
  type: "Scale"
  bottom: "conv1/bn_p"
  top: "scale1_p"
  scale_param {
    bias_term: true
  }
}

layer {
  name: "relu1_p"
  type: "TanH"
  bottom: "scale1_p"
  top: "scale1_p"
}
layer {
  name: "pool1_p"
  type: "Pooling"
  bottom: "scale1_p"
  top: "pool1_p"
  pooling_param {
    pool: MAX
    kernel_size: 3
    stride: 2
  }
}
layer {
  name: "conv2_p"
  type: "Convolution"
  bottom: "pool1_p"
  top: "conv2_p"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  convolution_param {
    num_output: 64
    pad: 2
    kernel_size: 5
    #stride: 2
    weight_filler {
      type: "xavier"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 0.1
    }
  }
}
layer {
  name: "conv2/bn_p"
  type: "BatchNorm"
  bottom: "conv2_p"
  top: "conv2/bn_p"
  param {
    lr_mult: 0
  }
  param {
    lr_mult: 0
  }
  param {
    lr_mult: 0
  }
}
layer {
  name: "scale2_p"
  type: "Scale"
  bottom: "conv2/bn_p"
  top: "scale2_p"
  scale_param {
    bias_term: true
  }
}
layer {
  name: "relu2_p"
  type: "TanH"
  bottom: "scale2_p"
  top: "scale2_p"
}
layer {
  name: "pool2_p"
  type: "Pooling"
  bottom: "scale2_p"
  top: "pool2_p"
  pooling_param {
    pool: MAX
    kernel_size: 3
    stride: 2
  }
}
layer {
  name: "conv3_p"
  type: "Convolution"
  bottom: "pool2_p"
  top: "conv3_p"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  convolution_param {
    num_output: 64
    pad: 2
    kernel_size: 5
    #stride: 2
    weight_filler {
      type: "xavier"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 0.1
    }
  }
}
layer {
  name: "conv3/bn_p"
  type: "BatchNorm"
  bottom: "conv3_p"
  top: "conv3/bn_p"
  param {
    lr_mult: 0
  }
  param {
    lr_mult: 0
  }
  param {
    lr_mult: 0
  }
}
layer {
  name: "scale3_p"
  type: "Scale"
  bottom: "conv3/bn_p"
  top: "scale3_p"
  scale_param {
    bias_term: true
  }
}
layer {
  name: "relu3_p"
  type: "TanH"
  bottom: "scale3_p"
  top: "scale3_p"
}
layer {
  name: "pool3_p"
  type: "Pooling"
  bottom: "scale3_p"
  top: "pool3_p"
  pooling_param {
    pool: MAX
    kernel_size: 3
    stride: 2
  }
}
layer {
  name: "conv4_p"
  type: "Convolution"
  bottom: "pool3_p"
  top: "conv4_p"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  convolution_param {
    num_output: 128
    kernel_size: 1
    weight_filler {
      type: "xavier"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 0.1
    }
  }
}
layer {
  name: "conv4/bn_p"
  type: "BatchNorm"
  bottom: "conv4_p"
  top: "conv4/bn_p"
  param {
    lr_mult: 0
  }
  param {
    lr_mult: 0
  }
  param {
    lr_mult: 0
  }
}
layer {
  name: "scale4_p"
  type: "Scale"
  bottom: "conv4/bn_p"
  top: "scale4_p"
  scale_param {
    bias_term: true
  }
}
layer {
  name: "relu4_p"
  type: "TanH"
  bottom: "scale4_p"
  top: "scale4_p"
}
layer {
  name: "pool4_p"
  type: "Pooling"
  bottom: "scale4_p"
  top: "pool4_p"
  pooling_param {
    pool: AVE
    kernel_size: 3
    stride: 2
  }
}
layer {
  name: "fc6_p"
  type: "InnerProduct"
  bottom: "pool4_p"
  top: "fc6_p"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  inner_product_param {
    num_output: 200
    weight_filler {
      type: "xavier"
      std: 0.005
    }
    bias_filler {
      type: "constant"
      value: 1
    }
  }
}
layer {
  name: "relu6_p"
  type: "TanH"
  bottom: "fc6_p"
  top: "fc6_p"
}
layer {
  name: "fc7_p"
  type: "InnerProduct"
  bottom: "fc6_p"
  top: "fc7_p"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  inner_product_param {
    num_output: 200
    weight_filler {
      type: "xavier"
      std: 0.005
    }
    bias_filler {
      type: "constant"
      value: 1
    }
  }
}
layer {
  name: "relu7_p"
  type: "TanH"
  bottom: "fc7_p"
  top: "fc7_p"
}

layer {
  name: "fc8_p"
  type: "InnerProduct"
  bottom: "fc7_p"
  top: "fc8_p"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  inner_product_param {
    num_output: 6
    weight_filler {
      type: "xavier"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 0
    }
  }
}
layer {
  name: "accuracy_proc"
  type: "Accuracy"
  bottom: "fc8_p"
  bottom: "labelproc"
  top: "accuracy_proc"
  include {
    phase: TEST
  }
}
layer {
  name: "loss_proc"
  type: "SoftmaxWithLoss"
  bottom: "fc8_p"
  bottom: "labelproc"
  top: "loss_proc"
  loss_weight: 1
}


