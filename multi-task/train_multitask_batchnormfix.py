import sys
import caffe

#script to train 2 task solver

#LIST OF BATCH NORMALIZATION LAYERS FOR SHARING - caffe batch normalization weight sharing is broken, this is a hot-fix
#remove layers that you don't want shared
L = [('conv1/bn','conv1/bn_p'),('scale1','scale1_p'),
     ('conv2/bn','conv2/bn_p'),('scale2','scale2_p'),
     ('conv3/bn','conv3/bn_p'),('scale3','scale3_p'),
     ('conv4/bn','conv4/bn_p'),('scale4','scale4_p')]

if __name__ == '__main__':
        caffe.set_device(0)
        caffe.set_mode_gpu()
        solver = caffe.SGDSolver('./models/solver_multitask_fc2.prototxt') #Choose the solver/model file that you want to use
	
	Niterations = 300000 #precomupted number of iterations
        for i in range(Niterations):
                sys.stdout.flush()

		#CONSTRAINED LAYER
            	tmp = solver.net.params['convF'][0].data*10000 #get contrained filter weights
            	tmp[:,:,2,2] = 0 #set middle value to 0
            	tmp = tmp.reshape((3,1,1,25))
            	tmp = tmp/tmp.sum(3).reshape((3,1,1,1)) #normalize
            	tmp = tmp.reshape((3,1,5,5))
            	tmp[:,:,2,2] = -1 #set middle value to 1
            	solver.net.params['convF'][0].data[...] = tmp #set constrained filter weights
            	solver.net.params['convF'][1].data[...] = 0
            	solver.step(1) #iterate

	        #Checks to make sure that layers through share depth match, and that layers above share depth are updating
                print 'convF: ' + str(solver.net.params['convF'][0].data[0][0][0]) + ' | convF_p: ' + str(solver.net.params['convF_p'][0].data[0][0][0])
            	print 'conv2bn: ' + str(solver.net.params['conv2/bn'][0].data[0]) + ' | conv2bn_p: ' + str(solver.net.params['conv2/bn_p'][0].data[0])
                print 'fc6: ' + str(solver.net.params['fc6'][0].data[0][0]) + ' | fc6_p: ' + str(solver.net.params['fc6_p'][0].data[0][0])


		#AVERAGE AND COPY LAYERS IN L (hotfix for batch norm sharing)
		for l in L:
		    for j in range(len(solver.net.params[l[0]])):
			d0 = solver.net.params[l[0]][j].data #get data from first side
			d1 = solver.net.params[l[1]][j].data #get data from other side
			d = (d0+d1)/2.0 #average them
			#copy into both sides
			solver.net.params[l[0]][j].data[...] = d
			solver.net.params[l[1]][j].data[...] = d
		
                print 'iteration ' + str(i+1) + ' is done'
