import sys
import caffe

if __name__ == '__main__':

    caffe.set_device(0) #for gpu training
    caffe.set_mode_gpu() #for gpu training

    solver = caffe.SGDSolver('./models/transfer_fc1_solver.prototxt') #SOLVER PROTOTXT - CHANGE THIS TO THE ONE YOU WANT	
    deployprototxt = './pretrained/deploy_single_mislnet.prototxt' #model file for baseline mislnet - needed for loading pre-trained weights
    caffemodel = 'TaskB_20camera_model_weights.caffemodel' #transfer weights learned for task B (camera model)
    #caffemodel = 'TaskA_6manipulations_model_weights.caffemodel' #transfer weights learned for task A (manipulation detection)

    refnet = caffe.Net(deployprototxt, caffemodel,caffe.TEST) #load pretrained model to memory
    
    #LAYERS TO COPY OVER FROM ref(erence) net to solver net
    L = ['convF','conv1','conv1/bn','scale1',
         'conv2','conv2/bn','scale2', 
         'conv3','conv3/bn','scale3',
         'conv4','conv4/bn','scale4','fc6','fc7']
         
    #COPY LAYERS IN L
    for l in L:
        for j in range(len(solver.net.params[l])):
            solver.net.params[l][j].data[...] = refnet.params[l][j].data #copy over weights from reference net to network we're learning

    Niterations = 120000 #precomputed number of iterations to train for

    for i in range(Niterations):
            sys.stdout.flush()

            tmp = solver.net.params['convF'][0].data*10000 #get contrained filter weights
            tmp[:,:,2,2] = 0 #set middle value to 0
            tmp = tmp.reshape((3,1,1,25))
            tmp = tmp/tmp.sum(3).reshape((3,1,1,1)) #normalize
            tmp = tmp.reshape((3,1,5,5))
            tmp[:,:,2,2] = -1 #set middle value to 1
            solver.net.params['convF'][0].data[...] = tmp #set constrained filter weights
            solver.net.params['convF'][1].data[...] = 0
            solver.step(1) #iterate
            
	    #Checks to make sure that layers through share depth match, and that layers above share depth are updating
            print 'conv4 ref: ' + str(refnet.params['conv4'][0].data[0][0]) + ' | train: ' + str(solver.net.params['conv4'][0].data[0][0])
            print 'conv4bn ref: ' + str(refnet.params['conv4/bn'][0].data[0]) + ' | train: ' + str(solver.net.params['conv4/bn'][0].data[0])
            print 'fc7 ref: ' + str(refnet.params['fc7'][0].data[0][0]) + ' | train: ' + str(solver.net.params['fc7'][0].data[0][0])
            print 'iteration ' + str(i+1) + ' is done'
