# Source code for: "Learning Unified Deep-Features for Multiple Forensic Tasks" 
by Owen Mayer, Belhassen Bayar, and Matthew C. Stamm  
Deparment of Electrical and Computer Engineering  
Drexel University - Philadelphia, PA, USA

## About

This repository contains a series of python scripts for training caffe models on multiple forensic tasks simultaneously. It also contains a number of pre-trained model weights that resulted from our experiments.

The scripts within this repository perform the following tasks:

- Transfer a trained network for one task, for use on another task (Transfer learning)
- Training on multiple tasks simultaneously (Multitask learning). There are models and scripts for 2 and 3 task learning, but it is a relatively straightforward extension to further tasks.
- Deep feature extraction from a pre-trained network
- Training an extremely-randomized-trees classifier on deep features

This code in this repository was used to conduct the experiments the article title "Learning Unified Deep-Features for Multiple Forensic Tasks," which was presented at The 6th ACM Workshop on Information Hiding and Multimedia Security, in Innsbruck, Austria, on 21 June 2018. 

- [Link to article](http://misl.ece.drexel.edu/wp-content/uploads/2018/04/Mayer_IHMMSec2018.pdf) (PDF)

**Note:** This repository does NOT contain the LMDB (image database) files that were used for training, as they are much too large to host on gitlab.

## Prerequisites

caffe and python 2.7 with the following packages:

- pycaffe
- lmdb
- numpy
- sklearn
- tqdm
- pandas

CUDA and cudNN required recommended for training on a GPU.

## Implementation Notes

Here are a couple of implementation details that may be useful while looking through the code.

### Batchnormalization hot-fix
For whatever reason there is an issue with weight sharing of batch-normalization layers in caffe. This was a known issue at the time we developed this code, and perhaps it is fixed now. However to address this, at each iteration we manually extract and average the batch-normalization scaling parameters for each network leg.

An example of this is found in **train_multitask_batchnormfix.py**:
```python
L = [('conv1/bn','conv1/bn_p'),('scale1','scale1_p'), #batch norm layers to be shared
     ('conv2/bn','conv2/bn_p'),('scale2','scale2_p'),
     ('conv3/bn','conv3/bn_p'),('scale3','scale3_p'),
     ('conv4/bn','conv4/bn_p'),('scale4','scale4_p')]

#AVERAGE AND COPY LAYERS IN L (hotfix for batch norm sharing)
for l in L:
    for j in range(len(solver.net.params[l[0]])):
    	d0 = solver.net.params[l[0]][j].data #get data from first side
    	d1 = solver.net.params[l[1]][j].data #get data from other side
    	d = (d0+d1)/2.0 #average them
    	#copy into both sides
    	solver.net.params[l[0]][j].data[...] = d
    	solver.net.params[l[1]][j].data[...] = d

```

### Naming conventions
There may be some discrepencies between the layer names used in the paper and what they are named in the caffe models. Here are the main differences:


| Layer name in Paper   | Layer Name in Caffe   |
| -------------         |:-------------         |
| Constr.               | ConvF                 |
| fc1                   | fc6                   |
| fc2                   | fc7                   |
| Output                | fc8                   |


## Citing this Code

If you are using this code for scholarly or academic research, please cite this paper:

Owen Mayer, Belhassen Bayar, and Matthew C. Stamm. "Learning Unified Deep-Features for Multiple Forensic Tasks." *The 6th ACM Workshop on Information Hiding and Multimedia Security* (2018).

bibtex:

```
@article{mayer2018unified,
  title={Learning Unified Deep-Features for Multiple Forensic Tasks},
  author={Mayer, Owen and Bayar, Belhassen and Stamm, Matthew C},
  journal={ACM Workshop on Information Hiding and Multimedia Security},
  year={2018}
}
```

