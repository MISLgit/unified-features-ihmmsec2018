import matplotlib.pyplot as plt

#import cv2
#import Image
import matplotlib
matplotlib.rcParams['backend'] = "Qt4Agg"
import numpy as np
import pandas as pd

from sklearn.ensemble import ExtraTreesClassifier
from sklearn.metrics import confusion_matrix

def train_test_et(ftrain,ftest,data_columns):

    dfTrain = pd.read_csv(ftrain)
    dfTest = pd.read_csv(ftest)

    feat_tr = dfTrain[data_columns].values #training data
    t_label = dfTrain['class'].values.astype(int) #training labels
    feat_tt = dfTest[data_columns].values #testing data
    t_y = dfTest['class'].values.astype(int) #training labels
    
    et = ExtraTreesClassifier(n_estimators=800, max_depth=None, min_samples_split=3, random_state=0, n_jobs=-1)
#    et = ExtraTreesClassifier(n_estimators=800, max_depth=None, min_samples_split=3, n_jobs=-1)
    et.fit(feat_tr, t_label)
    cet = et.predict(feat_tt)
    
    
    cmat5 = confusion_matrix(t_y, cet.tolist())
    nbr5 = cmat5.sum(1)
    nbr5 = np.array(nbr5, dtype = 'f')
    M5 = cmat5/nbr5
    np.set_printoptions(suppress=True)
    M5 = np.around(M5*100, decimals=2)
    
    acc = (cet == t_y).sum()/float(len(t_y))
    
    return acc,M5
    
if __name__ == "__main__":
    data_columns = np.array(range(200)).astype(str) #list of strings from '0' to '199' (indices of responses)

#    #FC1
#    ftrain = 'features/joint_traincam_fc1.csv.gz'
#    ftest = 'features/joint_valcam_fc1.csv.gz'
#    fout = 'results/confmat_joint_valcam_fc1.csv'
#    acc,M = train_test_et(ftrain,ftest,data_columns)
#    np.savetxt(fout, M, delimiter=',', fmt='%.2f')
#    print(fout + ' accuracy = ' + str(acc))
#    
#    ftrain = 'features/joint_trainproc_fc1.csv.gz'
#    ftest = 'features/joint_valproc_fc1.csv.gz'
#    fout = 'results/confmat_joint_valproc_fc1.csv'
#    acc,M = train_test_et(ftrain,ftest,data_columns)
#    np.savetxt(fout, M, delimiter=',', fmt='%.2f')
#    print(fout + ' accuracy = ' + str(acc))
#    
#    ftrain = 'features/single_cam_traincam_fc1.csv.gz'
#    ftest = 'features/single_cam_valcam_fc1.csv.gz'
#    fout = 'results/confmat_single_cam_valcam_fc1.csv'
#    acc,M = train_test_et(ftrain,ftest,data_columns)
#    np.savetxt(fout, M, delimiter=',', fmt='%.2f')
#    print(fout + ' accuracy = ' + str(acc))
#    
#    ftrain = 'features/single_cam_trainproc_fc1.csv.gz'
#    ftest = 'features/single_cam_valproc_fc1.csv.gz'
#    fout = 'results/confmat_single_cam_valproc_fc1.csv'
#    acc,M = train_test_et(ftrain,ftest,data_columns)
#    np.savetxt(fout, M, delimiter=',', fmt='%.2f')
#    print(fout + ' accuracy = ' + str(acc))
#    
#    ftrain = 'features/single_proc_traincam_fc1.csv.gz'
#    ftest = 'features/single_proc_valcam_fc1.csv.gz'
#    fout = 'results/confmat_single_proc_valcam_fc1.csv'
#    acc,M = train_test_et(ftrain,ftest,data_columns)
#    np.savetxt(fout, M, delimiter=',', fmt='%.2f')
#    print(fout + ' accuracy = ' + str(acc))
#    
#    ftrain = 'features/single_proc_trainproc_fc1.csv.gz'
#    ftest = 'features/single_proc_valproc_fc1.csv.gz'
#    fout = 'results/confmat_single_proc_valproc_fc1.csv'
#    acc,M = train_test_et(ftrain,ftest,data_columns)
#    np.savetxt(fout, M, delimiter=',', fmt='%.2f')
#    print(fout + ' accuracy = ' + str(acc))
#    
    #FC2
#    ftrain = 'features/base5cam3_train5cam3_fc2.csv.gz'
#    ftest = 'features/base5cam3_val5cam3_fc2.csv.gz'
#    fout = 'results/confmat_base5cam3_val5cam3_fc2.csv'
#    acc,M = train_test_et(ftrain,ftest,data_columns)
#    np.savetxt(fout, M, delimiter=',', fmt='%.2f')
#    print(fout + ' accuracy = ' + str(acc))

#    ftrain = 'features/base6proc_train5cam3_fc2.csv.gz'
#    ftest = 'features/base6proc_val5cam3_fc2.csv.gz'
#    fout = 'results/confmat_base6proc_val5cam3_fc2.csv'
#    acc,M = train_test_et(ftrain,ftest,data_columns)
#    np.savetxt(fout, M, delimiter=',', fmt='%.2f')
#    print(fout + ' accuracy = ' + str(acc))

    ftrain = 'features/basehybrid5cam2_train5cam2_fc2_bs40.csv.gz'
    ftest = 'features/basehybrid5cam2_val5cam2_fc2_bs40.csv.gz'
    fout = 'results/confmat_basehybrid5cam2_val5cam2_fc2_bs40.csv'
    acc,M = train_test_et(ftrain,ftest,data_columns)
    np.savetxt(fout, M, delimiter=',', fmt='%.2f')
    print(fout + ' accuracy = ' + str(acc))

    ftrain = 'features/basehybrid5cam2_train5cam2_fc2_bs5.csv.gz'
    ftest = 'features/basehybrid5cam2_val5cam2_fc2_bs5.csv.gz'
    fout = 'results/confmat_basehybrid5cam2_val5cam2_fc2_bs5.csv'
    acc,M = train_test_et(ftrain,ftest,data_columns)
    np.savetxt(fout, M, delimiter=',', fmt='%.2f')
    print(fout + ' accuracy = ' + str(acc))


