# -*- coding: utf-8 -*-
"""
Created on Mon Sep 18 17:25:14 2017

@author: owen
"""


import caffe
import numpy as np
import lmdb
import pandas as pd
from tqdm import tqdm
from scipy.stats import entropy
import argparse

parser = argparse.ArgumentParser(description='Feed-forward and extract features from layer')
parser.add_argument("deployproto",type=str) #path to caffe deploy prototxt
parser.add_argument("caffemodel",type=str) #path to saved model weights
parser.add_argument("lmdbpath",type=str) #path to lmdb file
parser.add_argument("outpath",type=str) #path to save activations
parser.add_argument("layer_name",type=str) #layer name to pull features from
args = parser.parse_args()

deployprototxt = args.deployproto
caffemodel = args.caffemodel
lmdbpath = args.lmdbpath
layer_name = args.layer_name
output_path = args.outpath

#deployprototxt = 'deploy_cam13_4fc_14sep2017.prototxt'
#caffemodel = 'snapshot_cam13_4fc_iter_200000.caffemodel'
#lmdbpath = '/home/owen/Research/Deep Learning/LMDBUtils/val1DID_13Cam_14Sep2017/'
#layer_name = 'fc8'
#output_path = 'cam13_4fc_fc8_ent.csv.gz'

net = caffe.Net(deployprototxt, caffemodel,caffe.TEST) #load network to memory
caffe.set_device(0)
caffe.set_mode_gpu()

lmdb_env = lmdb.open(lmdbpath) #open lmdb

lmdb_txn = lmdb_env.begin()
lmdb_cursor = lmdb_txn.cursor()
Nlmdb = int(lmdb_txn.stat()['entries'])#number of lmdb entries


ndata = net.blobs[layer_name].count #feature dimension

datacolumns = list(range(ndata)) #build feature vector labels
columns = ['key','class']
columns.extend(datacolumns)
dfActivations = pd.DataFrame(columns=columns)

listdf = []
chunksize = 10000

for key, value in tqdm(lmdb_cursor,desc='Extracting ' + layer_name,total=Nlmdb): #iterate through each patch in lmdb, tqdm is a wrapper for the progress indicator

    #load and format image
    datum = caffe.proto.caffe_pb2.Datum()
    datum.ParseFromString(value) 
    image = caffe.io.datum_to_array(datum) 
    image = image.astype(np.uint8)

    #feed-forward image patch, extract features
    activations = net.forward(data=np.asarray([image]),end=layer_name)

    #format output data
    listdata = [key,datum.label]
    listdata.extend(activations[layer_name][0].tolist())
    df = pd.DataFrame(data=[listdata],columns=columns)
    
    #append to dataframe
    listdf.append(df)
    if len(listdf)>=chunksize:
        dfActivations = pd.concat([dfActivations,pd.concat(listdf)])
        listdf = []

#save output
dfActivations.to_csv(output_path,compression='gzip')
